import pygame, time, random, os, sys
from pygame.locals import *
pygame.init()

#import file
menu = pygame.image.load("menu2.jpg")

state1 = pygame.image.load("grass.jpg")
state2 = pygame.image.load("brick2.jpg")
state3 = pygame.image.load("town.jpg")

hero = pygame.image.load("ironman2.gif")
support = pygame.image.load("teamiron.gif")
enemy1 = pygame.image.load("captain2.gif")
enemy2 = pygame.image.load("captain2.gif")
enemy3 = pygame.image.load("captain2.gif")
enemy4 = pygame.image.load("captain2.gif")
shield = pygame.image.load("SH.gif")
    
Bigfont = pygame.font.Font('thin_pixel-7.ttf', 120)
Normalfont = pygame.font.Font('thin_pixel-7.ttf', 50)
Smallfont = pygame.font.Font('thin_pixel-7.ttf', 40)  
class Charactor(pygame.sprite.Sprite):
    def __init__(self,picture):
        self.x = round(random.randrange(0, width - blocksize) /float(blocksize))*float(blocksize)
        self.y = round(random.randrange(0, height - blocksize) /float(blocksize))*float(blocksize)
        self.picture = picture
    def blit_charactor(self):
        return start.screen.blit(self.picture, (self.x, self.y))
class Enemy(pygame.sprite.Sprite):
    def __init__(self,direction):
        super().__init__()
        self.image = direction
        self.rect = self.image.get_rect()
        self.way = 'GO'
        if self.image == enemy1:
            self.direction = 'left' 
        elif self.image == enemy2:
            self.direction = 'right'    
        elif self.image == enemy3:
            self.direction = 'up'
        elif self.image == enemy4:
            self.direction = 'down'      
    def update(self):
        x = 0
        y = 0
        if self.direction == 'left':
            if self.way == 'GO':
                x = -10
                y = 0
            elif self.way == 'BACK':
                x = 10
                y = 0   
        elif self.direction == 'right' :
            if self.way == 'GO':
                x = 10
                y = 0        
            elif self.way == 'BACK':
                x = -10  
                y = 0                  
        elif self.direction == 'up' :
            if self.way == 'GO':
                x = 0 
                y = -10   
            elif self.way == 'BACK':
                x = 0 
                y = 10
        elif self.direction == 'down' :
            if self.way == 'GO':
                x = 0 
                y = 10      
            elif self.way == 'BACK':
                x = 0 
                y = -10

        self.rect.x += x 
        self.rect.y += y
        if self.rect.y >= start.height-10 or self.rect.y <= 10 or self.rect.x >= start.width-10 or self.rect.x <= 10:
            if self.way == 'BACK':
                self.way = 'GO'
            elif self.way == 'GO':
                self.way = 'BACK' 
class Attack(pygame.sprite.Sprite):
    def __init__(self, direction):
        super().__init__()
        self.direction = direction
        if direction == 'left':
            self.image = shield
        elif self.direction == 'right':
            self.image = shield
        elif self.direction == 'up':    
            self.image = shield
        elif self.direction == 'down':    
            self.image = shield
        self.rect = self.image.get_rect()
    def update(self):
        if self.direction == 'left':
            x = -50
            y = 0
        elif self.direction == 'right':
            x = 50
            y = 0   
        elif self.direction == 'up':
            x = 0
            y = -50
        elif self.direction == 'down':
            x = 0
            y = 50     
        self.rect.y += y
        self.rect.x += x                   
class Display:
    def __init__(self,width, height):
        self.width = width
        self.height = height
        self.screen = pygame.display.set_mode((width, height))
    def start_menu(self):
        pygame.display.set_caption('Ironman VS Captain America')
        startmenu = True
        while startmenu:
            name_game = Bigfont.render('Ironman VS Captain America', True, (255,255,255))
            detail = Smallfont.render('You are Ironman', True, (255,255,255))
            detail2 = Smallfont.render('you much to kill Captain America for your win', True, (255,255,255))
            detail3 = Smallfont.render('and you can keep your friends for help you kill Captain', True, (255,255,255))
            detail4 = Smallfont.render('when you press \'space bar\' you can attack Captain', True, (255,255,255))
            detail5 = Smallfont.render('prees P to play', True, (255,255,255))
            self.screen.blit(menu,(0,0))
            self.screen.blit(name_game, [40,5])
            self.screen.blit(detail, [370,100])
            self.screen.blit(detail2, [200,130])
            self.screen.blit(detail3, [150,160])
            self.screen.blit(detail4, [200,190])
            self.screen.blit(detail5, [370,550])
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    startmenu = False
                    pygame.quit()
                    quit()                 
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_p:
                        startmenu = False
                        self.run_game()
            pygame.display.update() 
    def run_game(self):
        changex = 0
        changey = 0
        blocksize = 50
        FPS = 7
        herolist = []
        heroLength = 1
        score = 0
        shieldx = 0
        shieldy = 0
        gameExit = False
        gameOver = False  
        
        clock = pygame.time.Clock()  

        all_sprites_list = pygame.sprite.Group()
        EnemyList = pygame.sprite.Group()
        AttackList = pygame.sprite.Group()
        HERO = Charactor(hero)
        SUPPORT = Charactor(support)
        ENEMY = Charactor(enemy1)    
        for i in range(3):
            type_enemy = [enemy1, enemy2, enemy3, enemy4]
            # This represents a block
            MOVEENEMY = Enemy(type_enemy[random.randrange(0,3)])
         
            # Set a random location for the block
            MOVEENEMY.rect.x = random.randrange(0+10,self.width-100)
            MOVEENEMY.rect.y = random.randrange(0+10,self.height-100)
         
            # Add the block to the list of objects
            EnemyList.add(MOVEENEMY)
            all_sprites_list.add(MOVEENEMY) 
        while not gameExit:
            while gameOver == True:
                if score<30:
                    start.OverScreen()   
                else:
                    self.screen.blit(menu,(0,0))
                    win = Bigfont.render('You Won!!', True, (255,255,255))
                    self.screen.blit(win, [340,100])
                    pygame.display.update()
                    for event in pygame.event.get():
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_q:
                                pygame.quit()
                                quit()                     
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    gameExit = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        changex = - blocksize
                        changey = 0
                        direct = 'Left'
                    elif event.key == pygame.K_RIGHT:
                        changex = blocksize
                        changey = 0
                        direct = 'Right'
                    elif event.key == pygame.K_UP:
                        changey = -blocksize
                        changex = 0
                        direct = 'Up'
                    elif event.key == pygame.K_DOWN:
                        changey = blocksize
                        changex = 0
                        direct = 'Down'
                        
                    #Attack Enemy
                    elif event.key == pygame.K_SPACE:
                        fire = True
                        fireX = int(HERO.x)
                        fireY = int(HERO.y)
                        startingShell = (HERO.x,HERO.y)
                        while fire:
                            for event in pygame.event.get():
                                if event.type == pygame.QUIT:
                                    pygame.quit()
                                    quit()
                            if direct == 'Left':
                                while fireX > 0 :
                                    pygame.draw.circle(self.screen,(255,0,0),(fireX,fireY+25),5)
                                    fireX -= 10
                                    pygame.display.update()
                                    clock.tick(10000)
                                    fire = False
                                    for spot in EnemyList:
                                        if (spot.rect.x == fireX and fireY < spot.rect.x + 50) or (spot.rect.x < fireX + blocksize and fireX + blocksize < spot.rect.x + 50):
                                            if (fireY > spot.rect.y and fireY < spot.rect.y + 50) or (fireY + blocksize > spot.rect.y and fireY + blocksize < spot.rect.y + 50):
                                                spot.rect.x = random.randrange(0+10,self.width-100)
                                                spot.rect.y = random.randrange(0+10,self.width-100)
                                                score+= 1
                            if direct == 'Right':
                                while fireX < width :
                                    pygame.draw.circle(self.screen,(255,0,0),(fireX+50,fireY+25),5)
                                    fireX += 10
                                    pygame.display.update()
                                    clock.tick(10000)   
                                    fire = False
                                    for spot in EnemyList:
                                        if (spot.rect.x == fireX and fireY < spot.rect.x + 50) or (spot.rect.x < fireX + blocksize and fireX + blocksize < spot.rect.x + 50):
                                            if (fireY > spot.rect.y and fireY < spot.rect.y + 50) or (fireY + blocksize > spot.rect.y and fireY + blocksize < spot.rect.y + 50):
                                                spot.rect.x = random.randrange(0+10,self.width-100)
                                                spot.rect.y = random.randrange(0+10,self.width-100)
                                                score+= 1
                            if direct == 'Up':
                                while fireY > 0 :
                                    pygame.draw.circle(self.screen,(255,0,0),(fireX+25,fireY),5)
                                    fireY -= 10
                                    pygame.display.update()
                                    clock.tick(10000)
                                    fire = False
                                    for spot in EnemyList:
                                        if (spot.rect.x == fireX and fireY < spot.rect.x + 50) or (spot.rect.x < fireX + blocksize and fireX + blocksize < spot.rect.x + 50):
                                            if (fireY > spot.rect.y and fireY < spot.rect.y + 50) or (fireY + blocksize > spot.rect.y and fireY + blocksize < spot.rect.y + 50):
                                                spot.rect.x = random.randrange(0+10,self.width-100)
                                                spot.rect.y = random.randrange(0+10,self.width-100)
                                                score+= 1                              
                            if direct == 'Down':
                                while fireY < self.height:
                                    pygame.draw.circle(self.screen,(255,0,0),(fireX+25,fireY+50),5)
                                    fireY += 10
                                    pygame.display.update()
                                    clock.tick(10000)   
                                    fire = False       
                                    for spot in EnemyList:
                                        if (spot.rect.x == fireX and fireY < spot.rect.x + 50) or (spot.rect.x < fireX + blocksize and fireX + blocksize < spot.rect.x + 50):
                                            if (fireY > spot.rect.y and fireY < spot.rect.y + 50) or (fireY + blocksize > spot.rect.y and fireY + blocksize < spot.rect.y + 50):
                                                spot.rect.x = random.randrange(0+10,self.width-50)
                                                spot.rect.y = random.randrange(0+10,self.width-50)
                                                score+= 1
            #when enermy out of area                                
            for spot in EnemyList:
                if spot.rect.x < 0 or spot.rect.x > width or spot.rect.y < 0 or spot.rect.y > height:
                    spot.rect.x = random.randrange(0+10,self.width-50)
                    spot.rect.y = random.randrange(0+10,self.width-50)
            #when Enemy come to center ,enemy will attack by shield
            for spot in EnemyList:
                if spot.rect.x >400 and spot.rect.x <450 :
                    attack = Attack(spot.direction)
                    attack.rect.x = spot.rect.x
                    attack.rect.y = spot.rect.y
                    AttackList.add(attack)                  
            #enemy walk
            EnemyList.update()
            AttackList.update()
            #hero walk
            HERO.x += changex
            HERO.y += changey 
                
            #when friend touch enemy
            for i in herolist:
                for spot in EnemyList:
                    if (spot.rect.x == i[0] and i[1] < spot.rect.x + 50) or (spot.rect.x < i[0] + blocksize and i[0] + blocksize < spot.rect.x + 50):
                        if (i[1] > spot.rect.y and i[1] < spot.rect.y + 50) or (i[1] + blocksize > spot.rect.y and i[1] + blocksize < spot.rect.y + 50):            
                            heroLength -=1  
                            del herolist[0]
                    
            #Friend follow
            herohead = []
            herohead.append(HERO.x)
            herohead.append(HERO.y)
            herolist.append(herohead)  
            if len(herolist) > heroLength:
                del herolist[0]
            for eachSegment in herolist[:-1]:
                if eachSegment == herohead:
                    gameOver = True
            #keep friend
            if HERO.x == SUPPORT.x and HERO.y == SUPPORT.y:
                SUPPORT.x = round(random.randrange(0,width-blocksize)/50.0)*50.0
                SUPPORT.y = round(random.randrange(0,height-blocksize)/50.0)*50.0
                heroLength +=1 
            #update screen
            if score < 10:
                self.screen.blit(state1,(0,0))
                screen_score = Normalfont.render('Score: %d/10' % score, True, (255,255,255))
                self.screen.blit(screen_score, [740,5])                
            elif score>=10 and score <20:
                self.screen.blit(state2,(0,0))
                screen_score = Normalfont.render('Score: %d/20' % score, True, (255,255,255))
                self.screen.blit(screen_score, [730,5]) 
            elif score >= 20 and score <30:
                self.screen.blit(state3,(0,0))
                screen_score = Normalfont.render('Score: %d/30' % score, True, (255,255,255))
                self.screen.blit(screen_score, [730,5])
            elif score >=30:
                gameOver = True                 
            SUPPORT.blit_charactor()
            EnemyList.draw(self.screen)
            AttackList.draw(self.screen)
            #Friend follow
            for XnY in herolist:
                self.screen.blit(support,(XnY[0],XnY[1]))        
            HERO.blit_charactor() 
            
            #touch the enemy
            for spot in EnemyList:
                if (spot.rect.x == HERO.x and HERO.y < spot.rect.x + 50) or (spot.rect.x < HERO.x + blocksize and HERO.x + blocksize < spot.rect.x + 50):
                    if (HERO.y > spot.rect.y and HERO.y < spot.rect.y + 50) or (HERO.y + blocksize > spot.rect.y and HERO.y + blocksize < spot.rect.y + 50):
                        gameOver = True
            #touch the sheild
            for spot in AttackList:
                if (spot.rect.x == HERO.x and HERO.y < spot.rect.x + 50) or (spot.rect.x < HERO.x + blocksize and HERO.x + blocksize < spot.rect.x + 50):
                    if (HERO.y > spot.rect.y and HERO.y < spot.rect.y + 50) or (HERO.y + blocksize > spot.rect.y and HERO.y + blocksize < spot.rect.y + 50):
                        gameOver = True            
            #Hit the wall
            if HERO.x > width-blocksize or HERO.x < 0 or HERO.y > height-blocksize or HERO.y < 0 or (HERO.x and HERO.y)% blocksize !=0 or (HERO.x and HERO.y)% blocksize !=0:
                gameOver = True 

            pygame.display.update()
            clock.tick(FPS)
    def OverScreen(self):
        self.screen.fill((0,0,0))
        screen_over = Normalfont.render('Game Over', True, (255,255,255))
        screen_over2 = Normalfont.render('press C to play again or Q to quit', True, (255,255,255))
        self.screen.blit(screen_over, [390,240])
        self.screen.blit(screen_over2, [220,300])
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    pygame.quit()
                    quit() 
                if event.key == pygame.K_c:
                    self.run_game()        
if __name__ ==  '__main__':
    global blocksize
    global direct
    blocksize = 50
    width = 900
    height = 600 
    start = Display(width,height)
    pygame.display.update()  
    start.start_menu()     